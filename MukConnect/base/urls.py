from . import views
from django.urls import path

urlpatterns = [
    path('',views.home,name='home'),
    path('login',views.login_user,name='login'),
    path('logout',views.logout_user,name='logout'),
    path('signup',views.signup,name='signup'),
    path('booking',views.booking,name='booking'),
    path('hostel_detail/<int:pk>/',views.hostel_detail,name='hostel_detail'),
    
]
