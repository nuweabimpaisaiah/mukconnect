from django.shortcuts import render,redirect
from django.contrib.auth import login,logout
from django.contrib import auth
from django.contrib import messages
from django.contrib.auth.models import User
from . forms import *
from . models import*

# Create your views here.

def home(request):
    if request.user.is_authenticated:
        user = request.user
        return render(request, 'base/home.html', {'user': user})
    else:
        messages.success(request,'You are Currently logged out')
        return redirect('login')
    

def login_user(request):
    if request.method=='POST':
        username=request.POST['username']
        password=request.POST['password']
        user=auth.authenticate(username=username,password=password)
        if user is not None:
            auth.login(request,user)
            messages.success(request,'You have been logged in successfully')
            return redirect('home')
        else:
            messages.error(request,'Invalid Credentials, Please try again')
            return redirect('login')
    else:
        return render(request,'base/login.html')


def logout_user(request):
    logout(request)
    return redirect('login')

def signup(request):
    if request.method=='POST':
        form=UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request,'Your Account has been created successfully')
            
           
            return redirect('login')
        else:
            for msg in form.error_messages:
                messages.error(request,f'{msg}: {form.error_messages[msg]}')
            return render(request,'base/signup.html',{'form':form})
    else:
        form=UserCreationForm()

    return render(request,'base/signup.html',{'form':form})





def booking(request):
    universities = University.objects.all()
    q = request.GET.get('q')
    hostels = Hostel.objects.filter(name=q)

    university_areas_hostels = {}
    for university in universities:
        areas = university.area_set.all()
        area_hostels = {}
        for area in areas:
            area_hostels[area] = area.hostel_set.all()
        university_areas_hostels[university] = area_hostels

    return render(request, 'base/bookings.html', {'university_areas_hostels': university_areas_hostels, 'selected_hostel': hostels.first()})


def hostel_detail(request,pk):
    hostel=Hostel.objects.get(id=pk)
    return render(request,'base/hostel_detail.html',{'hostel':hostel})

