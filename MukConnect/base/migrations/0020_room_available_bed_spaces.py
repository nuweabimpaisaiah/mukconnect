# Generated by Django 5.0.3 on 2024-04-29 10:01

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("base", "0019_hostel_free_rooms"),
    ]

    operations = [
        migrations.AddField(
            model_name="room",
            name="available_bed_spaces",
            field=models.IntegerField(default=0),
        ),
    ]
