# Generated by Django 5.0.3 on 2024-04-24 20:44

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("base", "0004_hostel_name"),
    ]

    operations = [
        migrations.AddField(
            model_name="hostel",
            name="image_1",
            field=models.ImageField(default="", upload_to="hostel_pictures"),
        ),
        migrations.AddField(
            model_name="hostel",
            name="image_2",
            field=models.ImageField(default="", upload_to="hostel_pictures"),
        ),
        migrations.AddField(
            model_name="hostel",
            name="image_3",
            field=models.ImageField(default="", upload_to="hostel_pictures"),
        ),
    ]
