from django.db import models

# Create your models here.

class University(models.Model):
    name=models.CharField(max_length=200)

    class Meta:
        verbose_name_plural='Universities'
    
    def __str__(self):
        return self.name

class Area(models.Model):
    university=models.ForeignKey(University,on_delete=models.CASCADE,default='')
    name=models.CharField(max_length=200)

    def __str__(self):
        return self.name
    
class Hostel(models.Model):
    area=models.ForeignKey(Area,on_delete=models.CASCADE,default='')
    name=models.CharField(max_length=200,blank=True)
    image_1=models.ImageField(upload_to='hostel_pictures',default='',blank=True,null=True)
    image_2=models.ImageField(upload_to='hostel_pictures',default='',blank=True,null=True)
    image_3=models.ImageField(upload_to='hostel_pictures',default='',blank=True,null=True)
    mtn_contact=models.CharField(max_length=20,blank=True,null=True)
    airtel_contact=models.CharField(max_length=20,blank=True,null=True)
    email=models.EmailField(default='',blank=True,null=True)
    is_full=models.BooleanField(default=False)
    free_rooms = models.IntegerField(default=0)


    def __str__(self):
        return self.name

class RoomCapacity(models.Model):
    name=models.CharField(max_length=20)

    def __str__(self):
        return self.name

    
class Room(models.Model):
    hostel=models.ForeignKey(Hostel,on_delete=models.CASCADE,default='')
    room_number=models.CharField(max_length=20,default='')
    capacity=models.ForeignKey(RoomCapacity,on_delete=models.CASCADE,default='')
    is_self_contained=models.BooleanField(default=False)
    price=models.IntegerField(default=0)
    booking_fee=models.IntegerField(default=0)
    is_occupied=models.BooleanField(default=False)
    available_bed_spaces=models.IntegerField(default=0)

    def __str__(self):
        return self.room_number

