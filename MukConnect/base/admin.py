from django.contrib import admin
from . models import *

# Register your models here.
admin.site.register(University)
admin.site.register(Area)
admin.site.register(Hostel)
admin.site.register(Room)
admin.site.register(RoomCapacity)

